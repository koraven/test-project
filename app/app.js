
const express = require('express') 
const path = require('path') 
const app = express() 
  
// Static Middleware 
app.use(express.static(path.join(__dirname, 'public'))) 

app.get('/', function(req, res){ 
    res.render('Demo') 
}) 
const MYVAR = process.env.MYVAR || 'default';  
app.listen(8080, function(error){ 
    if(error) throw error 
    console.log("Server created Successfully with variable: " + MYVAR) 
}) 
